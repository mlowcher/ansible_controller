# Ansible Controller
This repository is used to populate new AAP clusters with content. Follow these steps.

1. Create this repository as a project within the AAP controller.
2. Rename the default "Demo Inventory" to "zDemo Inventory". I do this as I like the demo inventory to be at the bottom of my list of inventories. It will be referenced in other playbooks.

Add resources in this order:
Organizations
Teams
Execution Environments
Credential_types
Credentials
Projects
Inventories
Templates

Add controller_ip: as a host variable to localhost
